require_dependency "subscribem/application_controller"

module Subscribem
  class AccountsController < ApplicationController

    def new
      authorize current_account, :super_admin_new?
      @account = Subscribem::Account.new
      @account.build_owner
    end

    def create
      authorize current_account, :super_admin_new?
      @account = Subscribem::Account.create_with_owner(account_params)
      if @account.valid?
        flash[:success] = "Your account has been successfully created."
        subdomain = request.subdomain.eql?('admin') ? 'admin': @account.subdomain
        redirect_to subscribem.root_url(:subdomain => subdomain)
      else
        flash[:error] = "Sorry, your account could not be created."
        render :new
      end
    end

    private
    def account_params
      params.require(:account).permit(:name, :subdomain, {:owner_attributes => [:email, :password, :password_confirmation]})
    end
  end
end
