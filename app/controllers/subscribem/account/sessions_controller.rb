require_dependency "subscribem/application_controller"

module Subscribem
  class Account::SessionsController < ApplicationController
    
    before_action :is_user_signed_in, only: [:new]

    def new
      @user = User.new
    end

    def create
      if env["warden"].authenticate(:scope => :user)
        flash[:notice] = "You are now signed in."
        redirect_to root_path
      else
        @user = User.new
        flash[:error] = "Invalid email or password."
        render :action => "new"
      end
    end

    def destroy
      env['warden'].logout
      redirect_to '/sign_in', notice: "Logged out!"
    end

    private

    def is_user_signed_in
      if current_user
        redirect_to root_url
      end
    end

  end
end
