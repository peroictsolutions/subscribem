require_dependency "subscribem/application_controller"

module Subscribem
  class Account::UsersController < ApplicationController

    def new
      authorize current_account, :owner_admin_new?
      @user = Subscribem::User.new
    end

    def create
      authorize current_account, :owner_admin_new?
      account = Subscribem::Account.where(subdomain: request.subdomain).first
      user = account.users.create(user_params)
      flash[:success] = "You have signed up successfully"
      redirect_to root_path
    end

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
  end
end
